# task_16_08_js

> IO program

> [Open App on website](https://enigmatic-wave-96466.herokuapp.com/)


## Build Setup

``` bash
# install dependencies
yarn install

# start by nodeJs at localhost:7777
yarn start

# serve with hot reload at localhost:8080
yarn run dev

# build for production with minification
yarn run build

# build for production and view the bundle analyzer report
yarn run build --report
```
