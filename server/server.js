const express = require('express'),
      path = require('path');

const app = express(),
      port = process.env.PORT || 7777;

// CONFIGURE OUR SERVER AND CLIENT SIDE TO RUN TOGETHER
// MAIN CATCHALL ROUTE
// ==============================================================
if(process.env.NODE_ENV !== 'production') {
  const webpackMiddleware = require('webpack-dev-middleware'),
    webpack = require('webpack');

  const webpackConfig = require(path.join(__dirname, '../build/webpack.dev.conf.js'));

  app.use(webpackMiddleware(webpack(webpackConfig)));
} else {
  app.use(express.static('dist'));
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../dist/index.html'));
  });
}
// ______________________________________________________________
app.use(express.static('dist'));

// START THE SERVER
// ==============================================================
app.listen(port, () => console.log(`Server is up on port ${port}`));
// ______________________________________________________________
