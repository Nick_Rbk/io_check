import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Solution from '@/components/Solution';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/solution',
      name: 'Solution',
      component: Solution,
    },
  ],
  mode: 'history',
  linkActiveClass: 'active',
});
